#!/bin/bash
wifi=$(ls /etc/netctl | rofi -i -dmenu -p)
if [[ "" != "$wifi" ]]
then
	urxvt -e sudo netctl switch-to "$wifi"
fi
