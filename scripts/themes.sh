#!/bin/bash
themes=$(themer list && echo "random")
theme=$(echo $themes | rofi -dmenu -p "Select theme" -sep " " -sort -sorting-method levenshtein -columns 2)
echo $theme

if [ "random" = "$theme" ]
then
    randtheme
elif [ ! "" = "$theme" ]
then
	themer activate "$theme"
fi
	
