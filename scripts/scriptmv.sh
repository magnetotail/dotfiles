#!/bin/sh
if [[ $(id -u) -ne 0 ]] ; then echo "Please run as root" ; exit 1 ; fi
cp localusrbin/* /usr/local/bin
cp services/* /etc/systemd/system
