#!/bin/bash
sizeFile="/tmp/term_font_size"
if [ $1 = "+" ]; then
	direction="bigger"
elif [ $1 = "-" ]; then
	direction="smaller"
elif [ $1 = "0" ]; then
	direction="none"
	rm $sizeFile
else 
	echo "$1 is not a valid argument."
	exit
fi
echo $direction
font=$(cat "/home/sam/.Xdefaults" | grep -o -e "^[^\!].*xft:.*size=" | grep -o -e "xft:.*")
echo $font
if [ -f "$sizeFile" ]; then
	fontSize=$(cat $sizeFile)
else
	fontSize=$(cat "/home/sam/.Xdefaults" | grep -o -e "^[^\!].*xft:.*" | grep -o -e "[[:digit:]]*")
	touch $sizeFile
fi
echo $fontsize
if [ $direction = "bigger" ]; then
	fontSize=$(($fontSize + 1))
elif [ $direction = "smaller" ]; then
	fontSize=$(($fontSize - 1))
fi
echo $font
for term in /dev/pts/[0-9]*
do
	echo -e "\033]710;$font$fontSize\007" > $term
	echo  "710;$font$fontSize"
done
echo $fontSize > $sizeFile
exit
