#!/bin/bash
for file in $@; do
	if [ -d "$file" ]
	then
		continue
	fi
	time=$(date -r "$file" +%Y%m%d%H%M%S)
	echo $time
	filename=$(basename -- "$file")
	extension="${filename##*.}"
	echo $extension
	newFile="${file%$filename}""$time"".""$extension"
	echo $newFile
	mv $file $newFile
done;



